# Mini-Projet

The Mini-Projet has to be developed in *groups of two students*.

## Important

You have to write a basic README file as soon as possible, following the [provided template](Projet/README_model.md).
You need to rename this file as `README_name1_name2.md`.

## Datasets

Non exhaustive list where data sets may be chosen from:

- FR Data.gouv: https://www.data.gouv.fr/fr/
- Insee: https://www.insee.fr/fr/accueil
- Kaggle: https://www.kaggle.com/datasets
- US Data.gov: https://catalog.data.gov/dataset
- Our World in Data: https://ourworldindata.org/

## Guidelines

1. Justify the data set selection and described it
2. Formulate three questions about the selected data set
3. Discuss with the professors and choose one question among these
4. Propose a methodology to answer that question
5. Implement this methodology using Literate Programming
6. The report must be acessible online (in your git repository)

## Report

The report has to be written with Rstudio or Jupyter, using R or Python. You need to provide both the original file
(`*.Rmd` or `*.ipynb`) and a compiled file (`*.pdf` or `*.html`). Both files need to be placed in the `Projet` directory of
your git repository. The files must be named `projet_MSPL_name1_name2.extension` (for instance,
`projet_MSPL_Vincent_Cornebize.Rmd`).

The report must contain:
- Frontpage
  - _Title_
  - Student's name
- Table of contents
- Introduction
  - Context / Dataset description
    - How the dataset has been obtained?
  - Description of the question
- Methodology
  - Data clean-up procedures
  - Scientific workflow
  - Data representation choices
- Analysis in Literate Programming
- Conclusion
- References

## Important notes

The question might be from simple to complex. More the question is
simple, more deep should be the analysis.

## Groups

Your project defence will be composed of (strictly) 5 minutes of presentation
and 2 minutes of questions. The presentation support could be anything: if you
have a PDF, you have to provide it to us in advance, if you need your own
machine be prepared (setup time is part of your defence time).

| id | Group | Time  | Teams                                       |Theme |   Repository | Rapport
|----|-------|-------|----------------------------------------------|---------------|-------------------------|-----
